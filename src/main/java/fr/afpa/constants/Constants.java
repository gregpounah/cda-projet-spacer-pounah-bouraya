package fr.afpa.constants;
import java.awt.Dimension;

public interface Constants {
	
	//TITRE JEU
	public static final String TITLE = "Space without invaders";
	
	//TAILLE BOARD
	public static final int LARGEUR_JEU = 500;
	public static final int HAUTEUR_JEU = 500;
	public static final Dimension DIMENSION_JEU = new Dimension (LARGEUR_JEU,HAUTEUR_JEU);
	public static final Dimension DIMENSION_FENETRE = new Dimension (LARGEUR_JEU+20,(HAUTEUR_JEU+46));
	
	//TAILLE DU VAISSEAU
	public static final int SPACESHIP_HAUTEUR = 28;
	public static final int SPACESHIP_LARGEUR = 34;

	//TAILLE DES METEORITES 
	public static final int METEOR_COTE = 16;
		
	//TAILLE DES METEORITES FIRE
	public static final int SIMPLE_METEOR_COTE = 16;
		
	//TAILLE DES METEORITES SIMPLES
	public static final int FIRE_METEOR_COTE = 16;
		
	//TAILLE DES METEORITES SIMPLES
	public static final int ICE_METEOR_COTE = 16;
		
	//TAILLE DES METEORITES ICEBERG
	public static final int ICEBERG_METEOR_COTE = 32;
		
	//TAILLE DES METEORITES ICEBERG
	public static final int ZIGZAG_METEOR_COTE = 16;
	
	
	//VITESSE DE RAFRACHISSEMENT DU PANEL (EN MILLISECONDES)
	public static final int GAMESPEED = 10;
	
	//CHEMIN FICHIER
	public static final String PATH_FILE = "Scores/scores.txt";
	
	//IMAGE JEU
	public static final String SPACESHIP_IMAGE_PATH = "images/spaceship.png";
	public static final String SPACESHIPleft_IMAGE_PATH = "images/spaceshipleft.png";
	public static final String SPACESHIPright_IMAGE_PATH = "images/spaceshipright.png";
	public static final String SIMPLE_METEOR_IMAGE_PATH = "images/meteor.png";
	public static final String FIRE_METEOR_IMAGE_PATH = "images/firemeteor.png";
	public static final String ICE_METEOR_IMAGE_PATH = "images/icemeteor.png";
	public static final String BACKGROUD_IMAGE_PATH = "images/spaceBackground.jpg";
	public static final String EXPLOSION_PATH = "images/star.png";
	public static final String ZIGZAG_METEOR_IMAGE_PATH = "images/zigzagmeteor.png";
}
