package fr.afpa.dao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

import fr.afpa.beans.Partie;
import fr.afpa.constants.Constants;

public class WriteReadFile implements Constants {


	private Scanner scanner;
	private StringBuilder sb;
	private String SB ;
	

	/**
	 * Cette méthode va parcourir le fichier ligne par ligne avec le scanner, et
	 * concaténer avec le StringBuilder. Le résultat du Stringbuilder est converti
	 * en String, et renvoyé.
	 */
	public String ReadList(List<String> list) throws IOException {

		sb = new StringBuilder();
		SB = null;
		
		
		try {
		scanner = new Scanner(new FileReader(PATH_FILE));
			int i=0;
			
			while (scanner.hasNextLine()) {
				
			
			sb.append(scanner.nextLine());
			sb.append("\n");
			SB = sb.toString();
			list.add(SB);
			
			
			
			}
		
			
		} catch (FileNotFoundException e) {
			System.out.println("le fichier n'existe pas");
			e.printStackTrace();
		}
	
		return SB;
	}

	/**
	 * Cette méthode va ouvrir le fichier, et y écrire, en revenant à la ligne
	 * chaque itération
	 * 
	 */
	public void WriteFile(List<Partie> list) {

		FileWriter myWriter;

		try {

			myWriter = new FileWriter(PATH_FILE, true);
			myWriter.write(list + System.lineSeparator());
			
			myWriter.close();
		} catch (IOException e) {
			System.out.println("le fichier n'existe pas");
			e.printStackTrace();

		}
	}
}
