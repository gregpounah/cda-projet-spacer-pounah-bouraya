package fr.afpa.image;
import java.awt.Image;

import javax.swing.ImageIcon;

import fr.afpa.constants.Constants;

public class ImageFactory implements Constants {

	public static ImageIcon createImage(Images image) {
		
		ImageIcon imageIcon = null;
		
		
		switch (image) {
		case BACKGROUD:
			imageIcon = new ImageIcon(BACKGROUD_IMAGE_PATH);
			break;
		case SPACESHIP:
			imageIcon = new ImageIcon(SPACESHIP_IMAGE_PATH);
			break;
		case SPACESHIPLEFT:
			imageIcon = new ImageIcon(SPACESHIPleft_IMAGE_PATH);
			break;
		case SPACESHIPRIGHT:
			imageIcon = new ImageIcon(SPACESHIPright_IMAGE_PATH);
			break;
		case SIMPLE_METEOR:
			imageIcon = new ImageIcon(SIMPLE_METEOR_IMAGE_PATH);
			break;
			
		case FIRE_METEOR:
			imageIcon = new ImageIcon(FIRE_METEOR_IMAGE_PATH);
			break;
			
		case ICE_METEOR:
			imageIcon = new ImageIcon(ICE_METEOR_IMAGE_PATH);
			break;
			
		case EXPLOSION:
			imageIcon = new ImageIcon(new ImageIcon(EXPLOSION_PATH).getImage().getScaledInstance(60, 60, Image.SCALE_SMOOTH));
			break;	
			
		case ICEBERG_METEOR:
			imageIcon = new ImageIcon(new ImageIcon(ICE_METEOR_IMAGE_PATH).getImage().getScaledInstance(32, 32, Image.SCALE_SMOOTH));
			break;
			
		case ZIGZAG_METEOR:
			imageIcon = new ImageIcon(ZIGZAG_METEOR_IMAGE_PATH);
			break;
			
		default:
			return null;
		}
		
		return imageIcon;
	}
}
