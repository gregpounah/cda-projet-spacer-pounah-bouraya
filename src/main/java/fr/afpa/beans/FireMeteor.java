package fr.afpa.beans;
import java.awt.Image;

import fr.afpa.constants.Constants;
import fr.afpa.image.ImageFactory;
import fr.afpa.image.Images;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FireMeteor extends Meteor implements Constants {

	private Image image;
	private int speed;
	private int life;
	private int point;
	private int type;
	private int cote;
	
	public FireMeteor() {
		
		this.image = ImageFactory.createImage(Images.FIRE_METEOR).getImage();
		this.speed = 8;
		this.life = 2;
		this.point = 1;
		this.cote = FIRE_METEOR_COTE;
	
	}
	
	@Override
	public boolean die() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void move() {
		// TODO Auto-generated method stub
		
		setPosY(getPosY()+getSpeed());
	} 

}
