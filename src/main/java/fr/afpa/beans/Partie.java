package fr.afpa.beans;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor

public class Partie implements Comparable<Partie> {

	private String NamePlayer;
	private int score;
	private LocalDateTime currentTime;

public Partie (String NamePlayer) {
		
		this.NamePlayer = NamePlayer;
		this.score = 0;
		this.currentTime = LocalDateTime.now();  
	}
	
	
	public Partie (String NamePlayer, int score) {
		
		this.NamePlayer = NamePlayer;
		this.score = 0;
		this.currentTime = LocalDateTime.now();  
	}

	@Override
	public int compareTo(Partie partie) {
		return score < partie.score ? 1 : score > partie.score ? -1 : 0;
	}
	
	@Override
	public String toString() {
		
		return NamePlayer+"   "+score+"   "+currentTime+"\n";
				
	}

}

