package fr.afpa.beans;
import java.awt.Image;

import fr.afpa.constants.Constants;
import fr.afpa.image.ImageFactory;
import fr.afpa.image.Images;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IcebergMeteor extends IceMeteor implements Constants {
	
	private Image image;
	private int speed;
	private int life;
	private int point;
	private int type;
	private int cote;
	
	public IcebergMeteor() {
		
		this.image = ImageFactory.createImage(Images.ICEBERG_METEOR).getImage();
		this.speed = 10;
		this.life = 2;
		this.point = 5;
		this.cote = ICEBERG_METEOR_COTE;
		
	}
	
	@Override
	public void move() {
		// TODO Auto-generated method stub
		
		setPosY(getPosY()+getSpeed());
	} 

}
