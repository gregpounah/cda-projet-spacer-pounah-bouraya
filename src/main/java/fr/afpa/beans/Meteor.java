package fr.afpa.beans;

import fr.afpa.constants.Constants;
import fr.afpa.control.Utils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Meteor extends AnimatedObjet implements Constants {
	
	private int posX;
	private int posY;
	private int cote;
	private boolean zigzag;
	
	public Meteor () {
		
	this.posX = Utils.randInt(30, LARGEUR_JEU-30);
	this.posY = -16;
	
	}		

	@Override
	public boolean die() {
		// TODO Auto-generated method stub
		return false;
	}

	public void move() {
		// TODO Auto-generated method stub
		setPosY(getPosY()+getSpeed());
		
	}
	
}
