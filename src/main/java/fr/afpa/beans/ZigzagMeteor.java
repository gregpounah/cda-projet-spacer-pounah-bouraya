package fr.afpa.beans;
import java.awt.Image;

import fr.afpa.constants.Constants;
import fr.afpa.control.Utils;
import fr.afpa.image.ImageFactory;
import fr.afpa.image.Images;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ZigzagMeteor extends Meteor implements Constants {
	
	private Image image;
	private int posX;
	private int speed;
	private int life;
	private int point;
	private int type;
	private int cote;
	private boolean zigzag;
	
	public ZigzagMeteor() {
		
		this.posX = Utils.randInt(200, LARGEUR_JEU-200);
		this.image = ImageFactory.createImage(Images.ZIGZAG_METEOR).getImage();
		this.speed = 8;
		this.life = 2;
		this.point = 5;
		this.cote = ZIGZAG_METEOR_COTE;
		this.zigzag = true;
	}


	@Override
	public boolean die() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void move() {
		// TODO Auto-generated method stub
		
		if (getPosY() < 125 && getPosX() > 30 && getPosX() < 470) {
		
			setPosY(getPosY()+getSpeed());
			setPosX(getPosX()+getSpeed());
		}
		
		if (getPosY() >= 125 && getPosY() < 350 && getPosX() > 30 && getPosX() < 470) {
			
			setPosY(getPosY()+getSpeed());
			setPosX(getPosX()-getSpeed());
		} 
		
		if (getPosY() >= 350 && getPosX() > 30 && getPosX() < 470) {
			
			setPosY(getPosY()+getSpeed());
			setPosX(getPosX()+getSpeed());
			
		}
	} 

}
