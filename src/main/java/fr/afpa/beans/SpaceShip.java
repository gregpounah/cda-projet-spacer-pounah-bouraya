package fr.afpa.beans;
import java.awt.Image;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import fr.afpa.constants.Constants;
import fr.afpa.image.ImageFactory;
import fr.afpa.image.Images;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class SpaceShip extends AnimatedObjet implements Constants {
		
	private ImageIcon imageIcon;
	private Image image;
	private Image imageleft;
	private Image imageright;
	private int life;
	private int posX;
	private int posY;
		
			
	public SpaceShip() {
		
		
		this.life = 5;
		this.imageIcon = ImageFactory.createImage(Images.SPACESHIP);
		this.image = ImageFactory.createImage(Images.SPACESHIP).getImage();
		this.imageleft = ImageFactory.createImage(Images.SPACESHIPLEFT).getImage();
		this.imageright = ImageFactory.createImage(Images.SPACESHIPRIGHT).getImage();
		this.posX = (LARGEUR_JEU / 2) - (SPACESHIP_LARGEUR / 2);
		this.posY = HAUTEUR_JEU - 150;
		
	}
	
	public Image explosion() {
		
		return image;
	}
	
	@Override
	public boolean die() {
		
		this.alive = false;
		
		return alive;
	}
	
	


}
