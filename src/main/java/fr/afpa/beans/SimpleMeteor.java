package fr.afpa.beans;
import java.awt.Image;

import fr.afpa.constants.Constants;
import fr.afpa.image.ImageFactory;
import fr.afpa.image.Images;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SimpleMeteor extends Meteor {

	private Image image;
	private int speed;
	private int life;
	private int point;
	private int type;
	private int cote;
	
	
	public SimpleMeteor() {
		
		this.image = ImageFactory.createImage(Images.SIMPLE_METEOR).getImage();
		this.speed = 10;
		this.life = 1;
		this.point = 2;
		this.cote = Constants.SIMPLE_METEOR_COTE;
	}
	

	@Override
	public boolean die() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void move() {
		
		setPosY(getPosY()+getSpeed());
		
	} 
}
