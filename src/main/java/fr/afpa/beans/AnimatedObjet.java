package fr.afpa.beans;

import java.awt.Image;

import javax.swing.ImageIcon;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AnimatedObjet {
	
	private int life;
	private ImageIcon imageIcon;
	private Image image;
	boolean alive = true;
	private int posX;
	private int posY;
	private int nextX;
	private int nextY;
	private int point;
	private int speed;
	private int type;
		
	public boolean  die() {
		
		alive = false;
		return alive;
		
	} 
	
	
	
}
