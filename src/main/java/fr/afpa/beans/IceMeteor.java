package fr.afpa.beans;
import java.awt.Image;

import fr.afpa.constants.Constants;
import fr.afpa.image.ImageFactory;
import fr.afpa.image.Images;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IceMeteor extends Meteor implements Constants {
	
	private Image image;
	private int speed;
	private int life;
	private int point;
	private int type;
	private int cote;
	
	public IceMeteor() {
		
		this.image = ImageFactory.createImage(Images.ICE_METEOR).getImage();
		this.speed = 10;
		this.life = 2;
		this.point = 3;
		this.cote = ICE_METEOR_COTE;
	}

	@Override
	public boolean die() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void move() {
		// TODO Auto-generated method stub
		
		setPosY(getPosY()+getSpeed());
	} 

}
