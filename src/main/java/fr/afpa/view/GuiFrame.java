package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JFrame;

import fr.afpa.constants.Constants;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter

public class GuiFrame extends JFrame implements Constants{
	
	GamePanel game;
	
	/**
	 * @author selim
	 * Methode de creation de la fenetre contenant le panel de jeu
	 */
	public GuiFrame() {
		
		this.setLayout(new BorderLayout());
		game = new GamePanel(this);
		add(game,BorderLayout.CENTER);
		setTitle(TITLE);
		setDefaultCloseOperation(EXIT_ON_CLOSE); 
		setLayout(new FlowLayout());
		add(game);
		pack();
		setSize(DIMENSION_FENETRE);
		setVisible(true);
		setLocation(450, 250);
		
	}
}
