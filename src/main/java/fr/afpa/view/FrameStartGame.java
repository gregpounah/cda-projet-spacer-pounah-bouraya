package fr.afpa.view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

import fr.afpa.constants.Constants;
import fr.afpa.image.ImageFactory;
import fr.afpa.image.Images;

public class FrameStartGame extends JFrame implements ActionListener, WindowListener, MouseListener, Constants {
	
	private Image backgroundImage;
	private String log;
	
	/**
	 * @AUTHOR selim
	 * METHODE CREATION NOUVEAU MESSAGE
	 */
	public FrameStartGame() {
		
		super(TITLE);
		backgroundImage = ImageFactory.createImage(Images.BACKGROUD).getImage();
		setContentPane(new ImagePanel(backgroundImage,log,this));
		addWindowListener(this);
		setSize(new Dimension(500,500));
		setVisible(true);
		pack();
		setVisible(true);
		setResizable(false);
		setSize(500, 500);
		setLocation(450, 250);
		setDefaultCloseOperation(EXIT_ON_CLOSE); 
	}
	
	public void paintComponent(Graphics g) {

		backgroundImage = ImageFactory.createImage(Images.BACKGROUD).getImage();
		g.drawImage(backgroundImage, 0, 0, LARGEUR_JEU, HAUTEUR_JEU, null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}
		
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}


}



	
	


