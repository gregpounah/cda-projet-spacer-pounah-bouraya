package fr.afpa.view;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JPanel;
import javax.swing.Timer;

import fr.afpa.beans.Meteor;
import fr.afpa.beans.SpaceShip;
import fr.afpa.constants.Constants;
import fr.afpa.control.Control;
import fr.afpa.image.ImageFactory;
import fr.afpa.image.Images;
import fr.afpa.models.GestionMeteor;
import fr.afpa.models.GestionShip;
import lombok.Setter;

@Setter

public class GamePanel extends JPanel implements ActionListener, WindowListener, KeyListener, Runnable, Constants {

	private Image background;
	private String name;
	private Image explosion;
	private Image imgship;
	private static SpaceShip falcon;
	private GestionMeteor gestionMeteor;
	private Meteor meteor1;
	private Meteor meteor2;
	private Meteor meteor3;
	private Meteor meteor4;
	private Timer timer;
	private Timer timerShip;
	private Timer timerMeteor;
	private GuiFrame guiFrame;
	private boolean on;
	int keyPressed;
	private static KeyEvent eventK;
	
	/**
	 * @author selim
	 * Methode d'initialisation du panel de jeu.
	 * Cette méthode appelle les méthodes de 
	 * construction du jeu et sera par lab suite 
	 * appelée par la classe GuiFrame
	 * @param guiFrame 
	 */
	public GamePanel(GuiFrame guiFrame) {
		
		this.guiFrame = guiFrame;
		setPanel();
					
	}

	//BUILD PANEL
	public void setPanel() {
		
		this.setOn(true);
		setLayout(new FlowLayout());
		setPreferredSize(DIMENSION_JEU);
		addKeyListener(this);
		this.setFocusable(true);
		
		background = ImageFactory.createImage(Images.BACKGROUD).getImage();
		explosion = ImageFactory.createImage(Images.EXPLOSION).getImage();
		imgship = ImageFactory.createImage(Images.SPACESHIP).getImage();
				
		//TIMER DE RAFRAICHISSEMENT DE L'AFFICHAGE DU PANEL
		timer = new Timer(GAMESPEED, new PanelRefresh(this));
		timer.start();
		
		//TIMER DE RAFRAICHISSEMENT DE L'AFFICHAGE DES METEORES SIMPLES
		gestionMeteor = new GestionMeteor(this);
		timerMeteor = new Timer(40, gestionMeteor);
		timerMeteor.start();
		
		//TIMER DE RAFRAICHISSEMENT DE L'AFFICHAGE DES MOUVEMENTS DU VAISSEAU
		timerShip  = new Timer(90, new GestionShip(this));
		timerShip.start();
		
		setVisible(true);
	}
	
	
	@Override
	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		g.drawImage(background, 0, 0, LARGEUR_JEU, HAUTEUR_JEU, null);
		doDrawing(g);
	}

	public void drawShip(Graphics g) {

		g.drawImage(falcon.getImage(), falcon.getPosX(), falcon.getPosY(), this);
	}
	
	public void drawMeteor(Graphics g) {
		
		g.drawImage(meteor1.getImage(),meteor1.getPosX(),meteor1.getPosY(),this);
		g.drawImage(meteor2.getImage(),meteor2.getPosX(),meteor2.getPosY(),this);	
		g.drawImage(meteor3.getImage(),meteor3.getPosX(),meteor3.getPosY(),this);
		g.drawImage(meteor4.getImage(),meteor4.getPosX(),meteor4.getPosY(),this);
		
	}
	public void drawExplosion(Graphics g) {

		
		g.drawImage(explosion, falcon.getPosX(), falcon.getPosY(), this);
	}
	public void drawScore(Graphics g) {
		
		g.setColor(Color.RED);
	    FontMetrics fm = g.getFontMetrics(g.getFont());
	    
	    g.drawString("Score : "+ gestionMeteor.getPartie().getScore() +"", 30, 25);
	  	
	}
	
	private void drawLife(Graphics g) {

		g.setColor(Color.RED);
		FontMetrics fm = g.getFontMetrics(g.getFont());
	    g.drawString("Life : "+ falcon.getLife(), 430, 25);
	}
	
	private void drawName(Graphics g) {
		
		g.setColor(Color.RED);
		FontMetrics fm = g.getFontMetrics(g.getFont());
		g.drawString(getName(), 240, 25);
	}
	
	public void doDrawing(Graphics g) {
		
		if(falcon.isAlive()) {
			drawShip(g);
		}
		drawMeteor(g);
		drawLife(g);
		drawScore(g);
		drawName(g);
		
		if(falcon.isAlive()!=true) {
		
		drawExplosion(g);	
		drawloose(g);
		
		}
		
	}
		
	public void doLoop() {
		
		repaint();
	}
	
	public void drawloose(Graphics g) {
		Font font = new Font("Verdana", Font.BOLD, 20);
	      g.setFont(font);
		g.setColor(Color.RED);
	    
	    g.drawString("Game Over", 210,250);
	  	
	}

	public void keyPressed(KeyEvent e) {
		

		keyPressed = e.getKeyCode();
		Control control = new Control();

		if (keyPressed == KeyEvent.VK_RIGHT || keyPressed == KeyEvent.VK_LEFT || keyPressed == KeyEvent.VK_UP || keyPressed == KeyEvent.VK_DOWN) {
		control.controlMovement(e, this.getFalcon(), this);
		}
	}		
	
	
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		keyPressed = e.getKeyCode();
		if (keyPressed == KeyEvent.VK_RIGHT || keyPressed == KeyEvent.VK_LEFT || keyPressed == KeyEvent.VK_UP || keyPressed == KeyEvent.VK_DOWN) {
			this.getFalcon().setImage(imgship);
			
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
	}

	public Meteor getMeteor1() {
		return meteor1;
	}

	public void setMeteor1(Meteor meteor1) {
		this.meteor1 = meteor1;
	}

	public Meteor getMeteor2() {
		return meteor2;
	}

	public void setMeteor2(Meteor meteor2) {
		this.meteor2 = meteor2;
	}

	public Meteor getMeteor3() {
		return meteor3;
	}

	public void setMeteor3(Meteor meteor3) {
		this.meteor3 = meteor3;
	}
	
	public Meteor getMeteor4() {
		return meteor4;
	}
	
	public void setMeteor4(Meteor meteor4) {
		this.meteor4 = meteor4;
	}

	public SpaceShip getFalcon() {
		return falcon;
	}

	public void setFalcon(SpaceShip falcon) {
		this.falcon = falcon;
	}

	public KeyEvent getEventK() {
		return eventK;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public GuiFrame getGuiFrame() {
		return guiFrame;
	}
	

	public boolean isOn() {
		return on;
	}
	

}
