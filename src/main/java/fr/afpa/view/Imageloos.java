package fr.afpa.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import fr.afpa.constants.Constants;

public class Imageloos extends JPanel implements ActionListener, TextListener, KeyListener, MouseListener, Constants {

	private Image img;
	private JLabel titreJeu;
	private JLabel question;
	private JPanel gridLayoutButton;
	private JButton jouer;
	private JButton score;
	private JButton quit;
	private FrameStartGame startgame;
	private YouLoosePanel youLoosePanel;
	private FrameScore framescore;

	public Imageloos(Image image, YouLoosePanel youLoosePanel, GamePanel game) {

		this.img = image;
		
		try {
			this.framescore = new FrameScore();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		this.youLoosePanel = youLoosePanel;

//		background  = ImageFactory.createImage(Images.BACKGROUD);

		// create Layout
		setLayout(new GridLayout(8, 1));

		add(Box.createHorizontalGlue());
		titreJeu = new JLabel("QUE FAIRE", SwingConstants.CENTER);
		titreJeu.setForeground(Color.RED);
		titreJeu.setFont(new Font("Bahnschrift", Font.BOLD, 30));
		titreJeu.setPreferredSize(new Dimension(200, 50));
		add(titreJeu);

		question = new JLabel("               ", SwingConstants.CENTER);
		question.setForeground(Color.WHITE);
		question.setFont(new Font("Bahnschrift", Font.BOLD, 15));
		question.setPreferredSize(new Dimension(300, 40));
		add(question);

		gridLayoutButton = new JPanel();
		gridLayoutButton.setLayout(new GridLayout(3, 1));
		gridLayoutButton.setBackground(Color.BLACK);
		add(gridLayoutButton);

		jouer = new JButton("jouer");
		jouer.setName("jouer");
		jouer.setPreferredSize(new Dimension(20, 40));
		jouer.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				game.getGuiFrame().setVisible(false);
				startgame = new FrameStartGame();
				// setVisible(false);
				setVisible(false);
				youLoosePanel.setVisible(false);
			}
		});

		gridLayoutButton.add(Box.createHorizontalGlue());
		gridLayoutButton.add(jouer);
		gridLayoutButton.add(Box.createHorizontalGlue());

		score = new JButton("Score");
		score.setName("Score");
		score.setPreferredSize(new Dimension(20, 40));
		score.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				framescore.setVisible(true);
			}
		});

		gridLayoutButton.add(Box.createHorizontalGlue());
		gridLayoutButton.add(score);
		gridLayoutButton.add(Box.createHorizontalGlue());

		quit = new JButton("Quitter");
		quit.setName("Quitter");
		quit.setPreferredSize(new Dimension(20, 40));
		quit.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});

		gridLayoutButton.add(Box.createHorizontalGlue());
		gridLayoutButton.add(quit);
		gridLayoutButton.add(Box.createHorizontalGlue());

		add(Box.createHorizontalGlue());
		add(Box.createHorizontalGlue());

	}

	
	public void paintComponent(Graphics g) {
		g.drawImage(img, 0, 0, null);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void textValueChanged(TextEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}
}
