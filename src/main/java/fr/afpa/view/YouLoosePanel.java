package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

import fr.afpa.constants.Constants;
import fr.afpa.image.ImageFactory;
import fr.afpa.image.Images;

public class YouLoosePanel extends JFrame implements ActionListener, WindowListener, TextListener, MouseListener, Constants {
	
	private Image background;
	private GamePanel game;
	
	public YouLoosePanel(GamePanel game) throws HeadlessException {
		super();
		this.game = game;
		setloosePanel();
	}
	
	public void setloosePanel() {
		
		setLayout(new BorderLayout());
		background = ImageFactory.createImage(Images.BACKGROUD).getImage();
		setContentPane(new Imageloos(background,this,game));
		addWindowListener(this);
		setSize(new Dimension(500,500));
		pack();
		setResizable(false);
		setSize(500, 500);
		setLocation(980, 250);
	}
	
	public void paintComponent(Graphics g) {

		background = ImageFactory.createImage(Images.BACKGROUD).getImage();
		//g.drawImage(background, 0, 0, LARGEUR_JEU, HAUTEUR_JEU, null);
	}
			
	@Override
	public void textValueChanged(TextEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		setVisible(false);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
