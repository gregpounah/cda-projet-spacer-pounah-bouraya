package fr.afpa.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PanelRefresh implements ActionListener {

	private GamePanel gamePanel;
	
	public PanelRefresh(GamePanel gamePanel) {
		this.gamePanel = gamePanel;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		gamePanel.doLoop();
		
	}

}