package fr.afpa.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import fr.afpa.constants.Constants;
import fr.afpa.control.Control;
import fr.afpa.image.ImageFactory;
import fr.afpa.image.Images;

public class ImagePanel extends JPanel implements ActionListener, TextListener, KeyListener, MouseListener{
	
	private JPanel gridCentral;
	private JPanel gridLayoutName;
	private JPanel gridLayoutButton;
	
	private JLabel titreJeu;
	private JLabel entrerNom;
	private JLabel backgroundLab;
	private ImageIcon background;
	private Image image;
	private JTextField name;
	private JButton jouer;
	public String nom;
	
	
	
	
	public ImagePanel(Image image , String log, FrameStartGame stargame) {

		this.image = image;

		
		background  = ImageFactory.createImage(Images.BACKGROUD);
		backgroundLab  = new JLabel(background);
		
		// create Layout
		setLayout(new GridLayout(8, 1));
        
		add(Box.createHorizontalGlue());
		titreJeu = new JLabel("<HTML><U>" + Constants.TITLE + "</U></HTML>", SwingConstants.CENTER);
		titreJeu.setForeground(Color.WHITE);
		titreJeu.setFont(new Font("Bahnschrift", Font.BOLD, 30));
		titreJeu.setPreferredSize(new Dimension(200, 50));
		add(titreJeu);

		entrerNom = new JLabel("Entrez votre nom (entre 3 et 6 lettres)", SwingConstants.CENTER);
		entrerNom.setForeground(Color.WHITE);
		entrerNom.setFont(new Font("Bahnschrift", Font.BOLD, 15));
		entrerNom.setPreferredSize(new Dimension(300, 40));
		add(entrerNom);

		name = new JTextField();
		name.setEditable(true);
		name.setPreferredSize(new Dimension(100,40));
		name.addActionListener(this);
		name.setForeground(Color.BLACK);
		
		gridLayoutName = new JPanel();
		gridLayoutName.setBackground(Color.BLACK);
		gridLayoutName.isForegroundSet();
		gridLayoutName.add(name);
		gridLayoutName.add(Box.createHorizontalGlue());

		gridCentral = new JPanel();
		gridCentral.setBackground(Color.BLACK);
		gridCentral.add(gridLayoutName);
		gridCentral.add(Box.createHorizontalGlue());
		add(gridCentral);
		add(Box.createHorizontalGlue());

		gridLayoutButton = new JPanel();
		gridLayoutButton.setLayout(new GridLayout(1, 3));
		gridLayoutButton.setBackground(Color.BLACK);
		add(gridLayoutButton);

		jouer = new JButton("jouer");
		jouer.setName("jouer");
		jouer.setPreferredSize(new Dimension(20,40));
		jouer.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
			
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				if (((JButton)e.getSource()).getName().contentEquals("jouer")) {
					
					Control control = new Control();
					control.controlName(name.getText());
					nom = name.getText();
					setVisible(false);
					stargame.setVisible(false);
					
				}
				
				else { System.out.println("Probleme d'envoi");}
			}
		});
		
		gridLayoutButton.add(Box.createHorizontalGlue());
		gridLayoutButton.add(jouer);
		gridLayoutButton.add(Box.createHorizontalGlue());

		
		add(Box.createHorizontalGlue());
		add(Box.createHorizontalGlue());
		log = nom;
	}
 
	public void paintComponent(Graphics g) {
    g.drawImage(image, 0, 0, null);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void textValueChanged(TextEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
}