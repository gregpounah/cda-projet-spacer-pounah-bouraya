package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

import fr.afpa.constants.Constants;
import fr.afpa.dao.WriteReadFile;
import lombok.Setter;

@Setter

public class Imagescore  extends JPanel implements ActionListener, TextListener, KeyListener, MouseListener, Constants {
	
	private Image img;
	private String score[] = new String[20];
	 
	private JList<String> list = new JList<>();
	private JTextArea message = new JTextArea("");
	private Border cadre = BorderFactory.createTitledBorder("Score :");
	DefaultListModel<String> model = new DefaultListModel();
	private JPanel pcenter;
	WriteReadFile reader = new WriteReadFile();
	private String listscore;
	private String fichescore;
	private Scanner scanner;
	
	
	public Imagescore(Image img, List<String> listchif) throws FileNotFoundException {
		super();
		scanner = new Scanner(new FileReader(PATH_FILE));
		
		fichescore = "";
		this.img = img;
		
		setLayout(new  GridLayout(1, 1));
		
		for(int i =0; i<20; i++) {
			
			score[i] = "";
		}
		
		
		try {
			
			
			listscore = reader.ReadList(listchif);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int j = 0;
		
		
		if(listscore != null) {
			
			for(int i=0; i<listscore.length(); i++) {
				
				if(j<20) {
				score[j] += listscore.charAt(i);
				if (listscore.charAt(i) == ']' && j<20) {
					j++;
				}
				}
		}
		
			
			
		}
		
		for (int i = 0; i<score.length; i++) {
			
			model.addElement(score[i]);
		
		}
		
		list.setFixedCellHeight(45);
	
		list.setModel(model);
		
		
		add(Box.createHorizontalGlue());
	
		this.add(list);
		add(Box.createHorizontalGlue());
	}

	public void paintComponent(Graphics g) {
		g.drawImage(img, 0, 0, null);
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void textValueChanged(TextEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}
