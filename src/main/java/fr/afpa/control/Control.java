package fr.afpa.control;

import java.awt.EventQueue;
import java.awt.event.KeyEvent;

import fr.afpa.beans.Partie;
import fr.afpa.beans.SpaceShip;
import fr.afpa.constants.Constants;
import fr.afpa.models.GestionPartie;
import fr.afpa.models.GestionShip;
import fr.afpa.view.FrameStartGame;
import fr.afpa.view.GamePanel;
import fr.afpa.view.GuiFrame;

public class Control {
	
	private int lengthName;
		

	public void controlMovement(KeyEvent e, SpaceShip falcon, GamePanel gamePanel) {

		GestionShip gs = new GestionShip(gamePanel);

		gs.move(e, falcon);
		
		
	}
	
	/**
	 * méthode de controle du nom permettant l'ouverture d'une Frame de jeu
	 * @param name
	 */
	public void controlName(String name) {
		
		SpaceShip falcon = new SpaceShip();
		lengthName = name.length();
				
		if(lengthName >=3 && lengthName <= 6) {
			
			if (!name.contains(";")) {
				
				  	EventQueue.invokeLater(() -> {
				GuiFrame gameFrame = new GuiFrame();
				
				if (gameFrame.getGame().getFalcon().isAlive()!=true) {
										
					gameFrame.getGame().setFalcon(falcon);
					/*gameFrame.getGame().getFalcon().setAlive(true);
					gameFrame.getGame().getFalcon().setLife(5);
					gameFrame.getGame().getFalcon().setPoint(0);
					gameFrame.getGame().getFalcon().setPosX((Constants.LARGEUR_JEU / 2) - (Constants.SPACESHIP_LARGEUR / 2));
					gameFrame.getGame().getFalcon().setPosY(Constants.HAUTEUR_JEU - 150);
					*/
				}
				gameFrame.getGame().setName(name);
				
				GestionPartie gestionPartie = new GestionPartie();
				gestionPartie.startPartie(name);					
				});
			}
				else {FrameStartGame start = new FrameStartGame();}		
			}
		else {FrameStartGame start = new FrameStartGame();
		
		}
	}
}
