package fr.afpa.models;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import fr.afpa.beans.SpaceShip;
import fr.afpa.constants.Constants;
import fr.afpa.image.ImageFactory;
import fr.afpa.image.Images;
import fr.afpa.view.GamePanel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GestionShip implements ActionListener, Constants {

	int start_x;
	int start_y;
	private SpaceShip falcon;
	private int keyPressed;
	private GamePanel gamePanel;
	private Image imageleft;
	private Image imageright;

	//Constructeur permettant d'instancier le vaissau et la méthode de déplacement, dans un Timer spécifique 
	public GestionShip(GamePanel gamePanel) {

		this.gamePanel = gamePanel;
		this.imageleft = ImageFactory.createImage(Images.SPACESHIPLEFT).getImage();
		this.imageright = ImageFactory.createImage(Images.SPACESHIPRIGHT).getImage();
		//permet de ne pas reinstancier un nouveau spaceShip en position 0 à chaque actionPerformed
		if(gamePanel.getFalcon() == null) {
		gamePanel.setFalcon(buildSpaceShip());
		}
		
		
		else {
		move(gamePanel.getEventK(), gamePanel.getFalcon());}
	}

	//Méthode d'instanciation du vaisseau
	public SpaceShip buildSpaceShip() {

		if(falcon == null) {
		SpaceShip falcon = new SpaceShip();
		this.falcon = falcon;
		}
		return falcon;
	}
		
	
	
	//Méthode de déplacement du vaisseau, dans les limites du panel de jeu
	public void move(KeyEvent e, SpaceShip falcon) {

		if (e!= null && falcon.isAlive()!=false) {
			
		keyPressed = e.getKeyCode();
		
					
			switch (keyPressed) {

			case KeyEvent.VK_LEFT:
				// permet de ne pas franchir le bord gauche
				
				if (falcon.getPosX() <= 0) {
					falcon.setPosX(0);
				} else {
					falcon.setImage(imageleft);
					falcon.setNextX(8);
					falcon.setPosX(falcon.getPosX() - falcon.getNextX());
					break;
				}

				break;

			case KeyEvent.VK_RIGHT:
				
			
				//falcon.setImage(imageright);
				// permet de ne pas franchir le bord droit
				if (falcon.getPosX() >= LARGEUR_JEU - SPACESHIP_LARGEUR) {
					falcon.setPosX(LARGEUR_JEU - SPACESHIP_LARGEUR);
				} else {
					falcon.setImage(getImageright());
					falcon.setNextX(-8);
					falcon.setPosX(falcon.getPosX() - falcon.getNextX());
					break;
				}

				break;

			case KeyEvent.VK_UP:
				// permet de ne pas franchir le haut du panel
				if (falcon.getPosY() <= 0) {
					falcon.setPosY(0);
					
					
				} else {
				
					falcon.setNextY(8);
					falcon.setPosY(falcon.getPosY() - falcon.getNextY());
					break;
				}

				break;

			case KeyEvent.VK_DOWN:
				// permet de ne pas franchir le bas du panel
				if (falcon.getPosY() >= HAUTEUR_JEU - SPACESHIP_HAUTEUR) {
					falcon.setPosY(HAUTEUR_JEU - SPACESHIP_HAUTEUR);
				} else {
					
					falcon.setNextY(-8);
					falcon.setPosY(falcon.getPosY() - falcon.getNextY());
					break;
				}
				break;

			default:
				break;

			}
			
			
			
		
		}
		
		
	}
	

	public void lifeSpaceShip() {

	}

	@Override
	public void actionPerformed(ActionEvent e) {

	
		
	}

}
