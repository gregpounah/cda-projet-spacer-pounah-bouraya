package fr.afpa.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.afpa.beans.Partie;
import fr.afpa.dao.WriteReadFile;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GestionPartie {
	
	WriteReadFile wr;
	Partie partie;

	private List <Partie> scores;

	/**
	 * //instanciation de la partie et 
	 * set de celle-ci vers l'objet Partie de la classe GstionMeteor
	 * @param name
	 */
	public void startPartie (String name) {
		
		partie = new Partie(name);
		GestionMeteor.setPartie(partie);	 
		
	}
	
	
	public void BuildList(Partie partie) {

		WriteReadFile wr = new WriteReadFile();
		
		scores = new ArrayList<Partie>();
		scores.add(partie);
		Collections.sort(scores);
		wr.WriteFile(scores);				
	}
}
