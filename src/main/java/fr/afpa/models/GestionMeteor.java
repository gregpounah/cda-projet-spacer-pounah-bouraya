package fr.afpa.models;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import fr.afpa.beans.FireMeteor;
import fr.afpa.beans.IceMeteor;
import fr.afpa.beans.IcebergMeteor;
import fr.afpa.beans.Meteor;
import fr.afpa.beans.Partie;
import fr.afpa.beans.SimpleMeteor;
import fr.afpa.beans.ZigzagMeteor;
import fr.afpa.constants.Constants;
import fr.afpa.control.Utils;
import fr.afpa.view.GamePanel;
import fr.afpa.view.YouLoosePanel;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter

public class GestionMeteor implements ActionListener, Constants {

	private Meteor meteor;
	private Meteor meteor1;
	private Meteor meteor2;
	private Meteor meteor3;
	private Meteor meteor4;
	private GamePanel gamePanel;
	private YouLoosePanel loos;
	private int randomInt;
	private static Partie partie;
	private GestionPartie gp;
		
	/**
	 * Constructeur instanciant les trois météorites simples et leur algorythme de
	 * déplacement et de réapparition, une fois le vaisseau touché ou la météorite
	 * sortie du panel de jeu
	 * 
	 * @param gamePanel
	 */
	public GestionMeteor(GamePanel gamePanel) {

		gp = new GestionPartie();
		//on génére un game panel identique à celui de la classe GameFrame et une nouvelle partie
		this.gamePanel = gamePanel;
		this.loos = new YouLoosePanel(gamePanel);
		
		//on instancie quatre météorites
		meteor1 = buildMeteor();
		meteor2 = buildMeteor();
		meteor3 = buildMeteor();
		meteor4 = buildMeteor();

		//on set quatre type different afin de pouvoir identifier l'objet et le reinstancier
		//dans la méthode build meteor
		meteor1.setType(1);
		meteor2.setType(2);
		meteor3.setType(3);
		meteor4.setType(4);

		//on set les quatres météorites qui seront manipulé dans la classe GamePanel
		gamePanel.setMeteor1(meteor1);
		gamePanel.setMeteor2(meteor2);
		gamePanel.setMeteor3(meteor3);
		gamePanel.setMeteor4(meteor3);
}

	/**
	 * Méthode de generation aléatoire de météore
	 * @return
	 */
	public Meteor buildMeteor() {

		randomInt = Utils.randInt(1, 5);
		
		switch (randomInt) {
		case 1:
			meteor = new SimpleMeteor();
			randomInt = Utils.randInt(1, 5);
			return meteor;

		case 2:
			meteor = new FireMeteor();
			randomInt = Utils.randInt(1, 5);
			return meteor;

		case 3:
			meteor = new IceMeteor();
			randomInt = Utils.randInt(1, 5);
			return meteor;

		case 4:
			meteor = new IcebergMeteor();
			randomInt = Utils.randInt(1, 5);
			return meteor;
			
		case 5:
			meteor = new ZigzagMeteor();
			randomInt = Utils.randInt(1, 5);
			return meteor;

		default:
			break;
		}
		return null;
	}

	/**
	 * méthode de déplacement et de réapparition des météorites
	 * Si la météorite est dans les limites du panel et n'a pas touché le vaisseau,
	 * on incrémente sa position Y de x pixels, correspondant à la vitesse de
	 * l'objet
	 * @param gamePanel
	 */
	public void lauchMeteor(GamePanel gamePanel, Meteor meteor) {
		
		// Si la météorite est dans les limites du panel, alors on incrémente sa
		// position Y de pixels correspondant à sa vitesse
		if (meteor.getLife() >= 0 && meteor.getPosY() < HAUTEUR_JEU && gamePanel.isOn()) {

			meteor.move();
			
			// Condition qui gère la collision entre les météore et le spaceship
			if (gamePanel.getFalcon().getPosY() + SPACESHIP_HAUTEUR >= (meteor.getPosY())

					&& gamePanel.getFalcon().getPosY() <= (meteor.getPosY() + meteor.getCote())

					&& gamePanel.getFalcon().getPosX() + SPACESHIP_LARGEUR >= meteor.getPosX()

					&& gamePanel.getFalcon().getPosX() <= meteor.getPosX() + meteor.getCote() && gamePanel.getFalcon().isAlive()) {

				// lorsque la meteore touche le vaisseau, on set son image à null afin de faire
				// disparaitre l'affichage
				meteor.setImage(null);
				meteor.die();
				
				//on décremente les points de vie du vaisseau en fonction des pv de la météore
				gamePanel.getFalcon().setLife(gamePanel.getFalcon().getLife() - meteor.getLife());
				meteor.setLife(0);
				
				//on instancie un nouveau météore
				buildNewMeteor(meteor);

				if (gamePanel.getFalcon().getLife() <= 0) {
					gamePanel.getFalcon().setLife(0);
					gamePanel.getFalcon().die();
					gp.BuildList(partie);
					gamePanel.setOn(false);
					loos.setVisible(true);
				}
			}
		}
		// Si la météore sort du panel, le score de la partie est incrémenté et un
		// nouveau météore est réinstanciée
		else  if (gamePanel.isOn()){
			
			if(gamePanel.getFalcon().isAlive() && meteor.isAlive()){
				partie.setScore(partie.getScore() + meteor.getPoint());
				}
			//meteor.setPoint(0);
			//meteor.setLife(0);
			buildNewMeteor(meteor);
		}
	}
	

	/**
	 * Methode de reconstruction d'une nouvelle météorite et de son lancement, une fois celle ci sortie du cadre
	 * ou entrée en colision avec le vaisseau
	 * @param meteor
	 */
	public void buildNewMeteor(Meteor meteor) {

		
		if (meteor.getType() == 1) {

			meteor = buildMeteor();
			meteor.setType(1);
			meteor1 = meteor;
			gamePanel.setMeteor1(meteor);
			lauchMeteor(gamePanel, meteor);

		}

		else if (meteor.getType() == 2) {

			meteor = buildMeteor();
			meteor.setType(2);
			meteor2 = meteor;
			gamePanel.setMeteor2(meteor);
			lauchMeteor(gamePanel, meteor);

		}

		else if (meteor.getType() == 3) {

			meteor = buildMeteor();
			meteor.setType(3);
			meteor3 = meteor;
			gamePanel.setMeteor3(meteor);
			lauchMeteor(gamePanel, meteor);
		}
		
		else if (meteor.getType() == 4) {
			
			meteor = buildMeteor();
			meteor.setType(4);
			meteor4 = meteor;
			gamePanel.setMeteor4(meteor);
			lauchMeteor(gamePanel, meteor);
			
		}
	}

	/**
	 *Lancement des quatres instances de déplacement des météorites
	 *cette méthode est appelée depuis le timerMeteor dans le GamePanel 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		
		lauchMeteor(gamePanel, meteor1);
		lauchMeteor(gamePanel, meteor2);
		lauchMeteor(gamePanel, meteor3);
		lauchMeteor(gamePanel, meteor4);
		
	}

	public Partie getPartie() {

		return partie;
	}
	
	public static void setPartie(Partie score) {
		partie = score;
	}
	
	
}
